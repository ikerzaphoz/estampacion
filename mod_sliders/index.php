<div id="sliders" class="row">
    <div id="myCarousel" class="carousel slide <?php if($page != "login"): echo "marginSliders"; endif;?>" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="<?=path_web?>/images/sliders/EST-DIG-1.jpg" alt="Img1">
            </div>

            <div class="item">
                <img src="<?=path_web?>/images/sliders/EST-DIG-2.jpg" alt="Img2">
            </div>

            <div class="item">
                <img src="<?=path_web?>/images/sliders/EST-DIG-3.jpg" alt="Img3">
            </div>

            <div class="item">
                <img src="<?=path_web?>/images/sliders/EST-DIG-4.jpg" alt="Img4">
            </div>


        </div>

        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
</div>