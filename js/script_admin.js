$(document).ready(function(){

    $('.image-popup-no-margins').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    function updateTagImage(){
        var value = $(this).val();
        var id_image = $(this).closest('tr').attr('id_image');

        var datas =
        {
            'valueTag':value,
            'id_image':id_image
        };
        $.ajax({
            url: "ajax/update_tags.php",
            type: "post",
            data: datas,
            dataType:'json',
            success: function (response) {
                console.log(response);
                if(response['error'] == 0){
                    toastr.success(response['message'], "Guardando datos");
                }
                else if(response['error'] == 1){
                    toastr.warning(response['message'], "Guardando datos");
                }
                else{
                    toastr.error(response['message'], "Guardando datos");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function updateCategoryImage(){
        var value = $(this).val();
        var id_image = $(this).closest('tr').attr('id_image');

        var datas =
        {
            'valueTag':value,
            'id_image':id_image
        };
        $.ajax({
            url: "ajax/update_category.php",
            type: "post",
            data: datas,
            dataType:'json',
            success: function (response) {
                console.log(response);
                if(response['error'] == 0){
                    toastr.success(response['message'], "Guardando datos");
                }
                else if(response['error'] == 1){
                    toastr.warning(response['message'], "Guardando datos");
                }
                else{
                    toastr.error(response['message'], "Guardando datos");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    $('.tokenfield_category').tagsInput({
        'defaultText': '',
        'onAddTag': updateCategoryImage,
        'onRemoveTag': updateCategoryImage,
        'delimiter': ','
    });

    $('.tokenfield').tagsInput({
        'defaultText': '',
        'onAddTag': updateTagImage,
        'onRemoveTag': updateTagImage,
        'delimiter': ','
    });

    function activeEdit(e){
        e.preventDefault();
        if($(this).prev().attr('readonly')){
            $(this).prev().removeAttr('readonly').focus().select().removeClass('tokendfield_readonly');
        }
        else{
            $(this).prev().attr('readonly', 'true').addClass('tokendfield_readonly');
        }
    }

    function adminChangeRef(e){
        e.preventDefault();

        var $this = $(this);
        var ref_old = $($this).parent().prev().val();
        var value = $($this).val();
        var id_image = $($this).closest('tr').attr('id_image');

        var dataString = 'valueRef='+value+'&id_image='+id_image+'&ref_old='+ref_old;

        $.ajax({
            url: "ajax/update_ref.php",
            type: "post",
            data: dataString,
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if(response['error'] == 0){
                    $($this).parent().prev().val(value);
                    toastr.success(response['message'], "Guardando datos");
                }
                else if(response['error'] == 1){
                    toastr.warning(response['message'], "Guardando datos");
                }
                else if(response['error'] == 2){
                    toastr.error(response['message'], "Guardando datos");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function pagination_admin(){

        var page = $(this).attr('data');
        var dataString = 'page='+page;

        $.ajax({
            type: "POST",
            url: "pagination.php",
            data: dataString,
            beforeSend: function(){
                $('.contain_table_images_admin').html('<div class="loading"></div>');
            },
            success: function(response) {
                console.log(response);
                $('.contain_table_images_admin').fadeIn(1000).html(response);
                $('img.lazy').lazyload();
                setTimeout(function(){
                    $('.tokenfield_category').tagsInput({
                        'defaultText': '',
                        'onAddTag': updateCategoryImage,
                        'onRemoveTag': updateCategoryImage,
                        'delimiter': ','
                    }),
                        $('.tokenfield').tagsInput({
                            'defaultText': '',
                            'onAddTag': updateTagImage,
                            'onRemoveTag': updateTagImage,
                            'delimiter': ','
                        })}, 10
                );

                setTimeout(function(){
                        $('.image-popup-no-margins').magnificPopup({
                            type: 'image',
                            closeOnContentClick: true,
                            closeBtnInside: false,
                            fixedContentPos: true,
                            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                            image: {
                                verticalFit: true
                            },
                            zoom: {
                                enabled: true,
                                duration: 300 // don't foget to change the duration also in CSS
                            }
                        });
                    }, 15
                );
            }
        });
    }

    function showModal(e){
        e.preventDefault();
        $('.modal-title').text("Categorías");
        $('#modal-basic').modal();
        $.ajax({
            type: "POST",
            url: "modal/modal_categories.php",
            success: function(response) {
                $('.content_modal_body').html(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    }

    $(document).on('click', '.activeEdit', activeEdit);
    $(document).on('change', '.changeRef', adminChangeRef);
    $(document).on('click', '.paginate_image_admin', pagination_admin);
    $(document).on('click', '.btn-modal', showModal);


});