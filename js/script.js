$(document).ready(function () {

    if ($('.ul_content_images_estampacion').length) {
        var page = "1";
        var li_image = "";
        var li_pagination = "";
        var total_hits = 0;
        var paginas = 0;
        var API_KEY = '5207830-59d7440da59fbb784b49e98c0';
        var URL = "https://pixabay.com/api/?key=" + API_KEY + "&safesearch=true&page=" + page + "&response_group=high_resolution&q=" + encodeURIComponent('Leones') + "&per_page=12&min_width=3000&min_height=3000";
        $.getJSON(URL, function (data) {
            total_hits = data.totalHits;
            paginas = Math.round(total_hits / 12);
            if (parseInt(data.totalHits) > 0) {
                $.each(data.hits, function (i, hit) {
                    li_image += "<li class='col-md-12'><a href='#'><img data-toggle='tooltip' \n\
data-placemente='top' placeholder='" + hit.id_hash + "'\n\
 title='" + hit.id_hash + "' ref-image='" + hit.id_hash + "' class='col-md-12 image_galery lazy' \n\
data-original='" + hit.webformatURL + "'/></a></li>";
                });
                $('.ul_content_images_estampacion').html(li_image);
                $('.lazy').lazyload();

                if (paginas >= 1) {
                    for (var i = 0; i < paginas; i++) {
                        var num_page = i + 1;
                        if (num_page == 1) {
                            li_pagination += "<li class='active'><a>" + num_page + "</a></li>";
                        } else {
                            li_pagination += "<li><a class='paginate paginate_image_gallery' data=" + num_page + ">" + num_page + "</a></li>";
                        }
                    }

                    $('.pagination_content_ul').html(li_pagination);
                }

            } else
                console.log('No hits');
        });
    }

    $('.lazy').lazyload();
    $('.colorpicker').colorpicker();

    function changeColorPicker() {
        var value = $(this).val() + " !important";
        $(this).css('background-color', value);
    }

    function selectImage() {
        $('.image_galery').closest('li').removeClass('active');
        $(this).closest('li').addClass('active');
        $('#estampacion_content_form').show();
        $('html, body').animate({
            scrollTop: $('#estampacion_content_form').offset().top},
                'slow');
        $('.ref_image').val($(this).attr('ref-image'));
        $('.src_image').val($(this).attr('src'));
    }

    $(document).on('click', '.image_galery', selectImage);

    function updateTagImage() {
        var value = $(this).val();
        var id_image = $(this).closest('tr').attr('id_image');

        var datas =
                {
                    'valueTag': value,
                    'id_image': id_image
                };
        $.ajax({
            url: "mod_images/ajax/update_tags.php",
            type: "post",
            data: datas,
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response['error'] == 0) {
                    toastr.success(response['message'], "Guardando datos");
                } else if (response['error'] == 1) {
                    toastr.warning(response['message'], "Guardando datos");
                } else {
                    toastr.error(response['message'], "Guardando datos");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function updateCategoryImage() {
        var value = $(this).val();
        var id_image = $(this).closest('tr').attr('id_image');

        var datas =
                {
                    'valueTag': value,
                    'id_image': id_image
                };
        $.ajax({
            url: "mod_images/ajax/update_category.php",
            type: "post",
            data: datas,
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if (response['error'] == 0) {
                    toastr.success(response['message'], "Guardando datos");
                } else if (response['error'] == 1) {
                    toastr.warning(response['message'], "Guardando datos");
                } else {
                    toastr.error(response['message'], "Guardando datos");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function loadContent() {
        var divLoad = $(this).attr('div_load');
        var pathLoad = $(this).attr('path_load');
        $(divLoad).load(pathLoad);

        setTimeout(function () {
            $('.tokenfield_category').tagsInput({
                'defaultText': '',
                'onAddTag': updateCategoryImage,
                'onRemoveTag': updateCategoryImage,
                'delimiter': ','
            }),
                    $('.tokenfield').tagsInput({
                'defaultText': '',
                'onAddTag': updateTagImage,
                'onRemoveTag': updateTagImage,
                'delimiter': ','
            })
        }, 15
                );

        setTimeout(function () {
            $('.image-popup-no-margins').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                fixedContentPos: true,
                mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                image: {
                    verticalFit: true
                },
                zoom: {
                    enabled: true,
                    duration: 300 // don't foget to change the duration also in CSS
                }
            });
        }, 10
                );


    }

    function pagination() {

        var texto = "Leones";
        if ($('#texto_busqueda').val().length) {
            texto = $('#texto_busqueda').val();
        }
        var page = $(this).attr('data');
        var li_image = "";
        var li_pagination = "";
        var total_hits = 0;
        var paginas = 0;
        var API_KEY = '5207830-59d7440da59fbb784b49e98c0';
        var URL = "https://pixabay.com/api/?key=" + API_KEY + "&safesearch=true&page=" + page + "&response_group=high_resolution&q=" + encodeURIComponent(texto) + "&per_page=12&min_width=3000&min_height=3000";
        $.getJSON(URL, function (data) {
            total_hits = data.totalHits;
            paginas = Math.round(total_hits / 12);
            if (paginas == 0) {
                paginas = 1;
            }
            if (parseInt(data.totalHits) > 0) {
                $.each(data.hits, function (i, hit) {
                    li_image += "<li class='col-md-12'><a href='#'><img data-toggle='tooltip' \n\
data-placemente='top' placeholder='" + hit.id_hash + "'\n\
 title='" + hit.id_hash + "' ref-image='" + hit.id_hash + "' class='col-md-12 image_galery lazy' \n\
data-original='" + hit.webformatURL + "'/></a></li>";
                });
                $('.ul_content_images_estampacion').html(li_image);
                $('.lazy').lazyload();

                for (var i = 0; i < paginas; i++) {
                    var num_page = i + 1;
                    if (page == num_page) {
                        li_pagination += "<li class='active'><a>" + num_page + "</a></li>";
                    } else {
                        li_pagination += "<li><a class='paginate paginate_image_gallery' data=" + num_page + ">" + num_page + "</a></li>";
                    }
                }

                $('.pagination_content_ul').html(li_pagination);


            } else
                console.log('No hits');
        });
    }

    function pagination_admin() {

        var page = $(this).attr('data');
        var dataString = 'page=' + page;

        $.ajax({
            type: "POST",
            url: "mod_images/pagination.php",
            data: dataString,
            beforeSend: function () {
                $('.contain_table_images_admin').html('<div class="loading"></div>');
            },
            success: function (response) {
                console.log(response);
                $('.contain_table_images_admin').fadeIn(1000).html(response);
                $('img.lazy').lazyload();
                setTimeout(function () {
                    $('.tokenfield_category').tagsInput({
                        'defaultText': '',
                        'onAddTag': updateCategoryImage,
                        'onRemoveTag': updateCategoryImage,
                        'delimiter': ','
                    }),
                            $('.tokenfield').tagsInput({
                        'defaultText': '',
                        'onAddTag': updateTagImage,
                        'onRemoveTag': updateTagImage,
                        'delimiter': ','
                    })
                }, 10
                        );

                setTimeout(function () {
                    $('.image-popup-no-margins').magnificPopup({
                        type: 'image',
                        closeOnContentClick: true,
                        closeBtnInside: false,
                        fixedContentPos: true,
                        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
                        image: {
                            verticalFit: true
                        },
                        zoom: {
                            enabled: true,
                            duration: 300 // don't foget to change the duration also in CSS
                        }
                    });
                }, 10
                        );
            }
        });
    }

    //marDer, marTop, anchoCortina, altoCortina
    function calculateImagePosition(x, y, xx, yy) {

    }

    function modalImage(e) {
        e.preventDefault();
        if ($('.ref_image').length) {
            var ancho = $(this).find('#ancho').val();
            var alto = $(this).find('#alto').val();
            if (ancho > 185 || alto > 185) {
                toastr.error("Una de las dos medidas debe de ser menor a 185cms", "Medidas");
            } else {
                $('.modal-title').text("Previsualización cortina");
                $('#modal-basic').modal();
                $.ajax({
                    url: "modals/content_modal_image.php",
                    type: "post",
                    data: $(this).serialize(),
                    success: function (response) {
                        $('.btn-enviar').removeClass('hidden');
                        $('.content_modal_body').html(response);
                        $('.colorpicker').colorpicker();

                        setTimeout(function () {
                            var width_canvas = $('.content_modal_body').width();
                            var height_canvas = $('#cortina').height();
                            var imagen_src = $('.image_canvas_src').val();


                            var imgSrc = $('.div_imagen_cortina').css('background-size');
                            var widthImage = imgSrc.split(" ")[0].replace('%', '');
                            var heightImage = imgSrc.split(" ")[1].replace('%', '');

                            //filtersImage();

                            /*$('#imagen').resizable({
                             handles: 'n,e,s,w,ne,se,sw,nw',
                             containment: '#cortina'
                             });
                             
                             $('#imagen').parent().css('width', '100%').css('height', '100%');
                             
                             $('#imagen').css('width', widthImage + '% ').css('height', heightImage + '%').css('min-width', widthImage + '% ').css('min-height', heightImage + '%');
                             
                             $('.draggable').draggable({
                             containment: '#cortina',
                             drag: function () {
                             var thisPos = $(this).position();
                             var parentPos = $(this).parent().position();
                             var x = thisPos.left - parentPos.left;
                             var y = thisPos.top - parentPos.top;
                             var anchoCortina = $('#ancho_cortina').val();
                             var altoCortina = $('#alto_cortina').val();
                             
                             calculateImagePosition(x, y, anchoCortina, altoCortina);
                             
                             }
                             });*/

                            /*$('.colorpicker').colorpicker();*/

                            (function () {
                                var $ = function (id) {
                                    return document.getElementById(id)
                                };

                                var canvas = this.__canvas = new fabric.Canvas('canvas', {
                                    isDrawingMode: true
                                });

                                fabric.Object.prototype.transparentCorners = false;

                                canvas.setHeight(height_canvas);
                                canvas.setWidth(width_canvas);
                                canvas.renderAll();

                                /*
                                 fabric.Image.fromURL(imagen_src, function (img) {
                                 // add background image
                                 canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                                 scaleX: canvas.width / img.width,
                                 scaleY: canvas.height / img.height
                                 });
                                 });
                                 */

                                fabric.Image.fromURL(imagen_src, function (img) {
                                    img.src = imagen_src;
                                    // add background image
                                    canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                                        scaleX: canvas.width / img.width,
                                        scaleY: canvas.height / img.height,
                                    });
                                }, null, {crossOrigin: 'anonymous'});

                                var drawingModeEl = $('drawing-mode'),
                                        drawingOptionsEl = $('drawing-mode-options'),
                                        drawingColorEl = $('drawing-color'),
                                        drawingLineWidthEl = $('drawing-line-width'),
                                        clearEl = $('clear-canvas');

                                clearEl.onclick = function () {
                                    canvas.clear()
                                    fabric.Image.fromURL(imagen_src, function (img) {
                                        img.src = imagen_src;
                                        // add background image
                                        canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                                            scaleX: canvas.width / img.width,
                                            scaleY: canvas.height / img.height,
                                        });
                                    }, null, {crossOrigin: 'anonymous'});
                                };

                                drawingModeEl.onclick = function () {
                                    canvas.isDrawingMode = !canvas.isDrawingMode;
                                    if (canvas.isDrawingMode) {
                                        drawingModeEl.innerHTML = 'Desactivar modo dibujo';
                                        drawingOptionsEl.style.display = '';
                                    } else {
                                        drawingModeEl.innerHTML = 'Activar modo dibujo';
                                        drawingOptionsEl.style.display = 'none';
                                    }
                                };

                                if (fabric.PatternBrush) {
                                    var vLinePatternBrush = new fabric.PatternBrush(canvas);
                                    vLinePatternBrush.getPatternSrc = function () {

                                        var patternCanvas = fabric.document.createElement('canvas');
                                        patternCanvas.width = patternCanvas.height = 10;
                                        var ctx = patternCanvas.getContext('2d');

                                        ctx.strokeStyle = this.color;
                                        ctx.lineWidth = 5;
                                        ctx.beginPath();
                                        ctx.moveTo(0, 5);
                                        ctx.lineTo(10, 5);
                                        ctx.closePath();
                                        ctx.stroke();

                                        return patternCanvas;
                                    };

                                    var hLinePatternBrush = new fabric.PatternBrush(canvas);
                                    hLinePatternBrush.getPatternSrc = function () {

                                        var patternCanvas = fabric.document.createElement('canvas');
                                        patternCanvas.width = patternCanvas.height = 10;
                                        var ctx = patternCanvas.getContext('2d');

                                        ctx.strokeStyle = this.color;
                                        ctx.lineWidth = 5;
                                        ctx.beginPath();
                                        ctx.moveTo(5, 0);
                                        ctx.lineTo(5, 10);
                                        ctx.closePath();
                                        ctx.stroke();

                                        return patternCanvas;
                                    };

                                    var squarePatternBrush = new fabric.PatternBrush(canvas);
                                    squarePatternBrush.getPatternSrc = function () {

                                        var squareWidth = 10, squareDistance = 2;

                                        var patternCanvas = fabric.document.createElement('canvas');
                                        patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
                                        var ctx = patternCanvas.getContext('2d');

                                        ctx.fillStyle = this.color;
                                        ctx.fillRect(0, 0, squareWidth, squareWidth);

                                        return patternCanvas;
                                    };

                                    var diamondPatternBrush = new fabric.PatternBrush(canvas);
                                    diamondPatternBrush.getPatternSrc = function () {

                                        var squareWidth = 10, squareDistance = 5;
                                        var patternCanvas = fabric.document.createElement('canvas');
                                        var rect = new fabric.Rect({
                                            width: squareWidth,
                                            height: squareWidth,
                                            angle: 45,
                                            fill: this.color
                                        });

                                        var canvasWidth = rect.getBoundingRect().width;

                                        patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
                                        rect.set({left: canvasWidth / 2, top: canvasWidth / 2});

                                        var ctx = patternCanvas.getContext('2d');
                                        rect.render(ctx);

                                        return patternCanvas;
                                    };

                                }

                                $('drawing-mode-selector').onchange = function () {

                                    if (this.value === 'Linea horizontal') {
                                        canvas.freeDrawingBrush = vLinePatternBrush;
                                    } else if (this.value === 'Linea vertical') {
                                        canvas.freeDrawingBrush = hLinePatternBrush;
                                    } else if (this.value === 'Cuadrado') {
                                        canvas.freeDrawingBrush = squarePatternBrush;
                                    } else if (this.value === 'Lapiz') {
                                        canvas.freeDrawingBrush = new fabric['PencilBrush'](canvas);
                                    } else {
                                        canvas.freeDrawingBrush = new fabric[this.value + 'Brush'](canvas);
                                    }

                                    if (canvas.freeDrawingBrush) {
                                        canvas.freeDrawingBrush.color = drawingColorEl.value;
                                        canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
                                    }
                                };

                                drawingColorEl.onchange = function () {
                                    canvas.freeDrawingBrush.color = this.value;
                                };
                                drawingLineWidthEl.onchange = function () {
                                    canvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
                                    this.previousSibling.innerHTML = this.value;
                                };

                                if (canvas.freeDrawingBrush) {
                                    canvas.freeDrawingBrush.color = drawingColorEl.value;
                                    canvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
                                }

                                $('save_image_canvas').onclick = function () {
                                    alert("GUARDAR IMAGEN");
                                    var dataUrl = canvas.toDataURL("image/png");
                                    var imageFoo = document.createElement('img');
                                    imageFoo.src = dataUrl;
                                   
                                    console.log(imageFoo);
                                }

                            })();

                        }, 1000);

                        //$('.div_imagen_cortina').resizable();

                        //canvas.add(new fabric.Circle({radius:30,fill:'#f55', top:2, left:2}));
                        //canvas.selectionColor = 'rgba(0,255,0,0.3)';
                        //canvas.selectionBorderColor = 'red';
                        //canvas.selectionLineWidth = 5;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("Error ajax en submitFormNewDni");
                        console.log(jqXHR, textStatus, errorThrown);
                    }

                });
            }
        } else {
            toastr.error("No has seleccionado ninguna imagen");
        }
    }

    function searchImages(e) {
        e.preventDefault();
        var texto = "Leones";
        if ($('#texto_busqueda').val().length) {
            texto = $('#texto_busqueda').val();
        }
        var page = "1";
        var li_image = "";
        var li_pagination = "";
        var total_hits = 0;
        var paginas = 0;
        var class_active = "";
        var API_KEY = '5207830-59d7440da59fbb784b49e98c0';
        var URL = "https://pixabay.com/api/?key=" + API_KEY + "&safesearch=true&page=" + page + "&response_group=high_resolution&q=" + encodeURIComponent(texto) + "&per_page=12&min_width=3000&min_height=3000";
        $.getJSON(URL, function (data) {
            total_hits = data.totalHits;
            paginas = Math.round(total_hits / 12);
            if (paginas == 0) {
                paginas = 1;
            }
            console.log(paginas);
            if (parseInt(data.totalHits) > 0) {
                $.each(data.hits, function (i, hit) {
                    li_image += "<li class='col-md-12'><a href='#'><img data-toggle='tooltip' \n\
data-placemente='top' placeholder='" + hit.id_hash + "'\n\
 title='" + hit.id_hash + "' ref-image='" + hit.id_hash + "' class='col-md-12 image_galery lazy' \n\
data-original='" + hit.webformatURL + "'/></a></li>";
                });
                $('.ul_content_images_estampacion').html(li_image);
                $('.lazy').lazyload();

                if (paginas >= "1") {
                    for (var i = 0; i < paginas; i++) {
                        var num_page = i + 1;
                        if (num_page == 1) {
                            class_active = 'active';
                        }
                        li_pagination += "<li class='" + class_active + "'><a class='paginate paginate_image_gallery' data=" + num_page + ">" + num_page + "</a></li>";
                    }

                    $('.pagination_content_ul').html(li_pagination);
                }

            } else
                console.log('No hits');
        });
    }

    function editLive() {
        var width = $('.ancho_imagen').text() / $('#ancho_cortina').val() * 100;
        var height = $('.alto_imagen').text() / $('#alto_cortina').val() * 100;
        $('#imagen').css('width', width + '% ').css('height', height + '%');
    }

    function changeEncuadre(e) {
        e.preventDefault();
        if ($('#imagen').hasClass('imagen_cortina50')) {
            $('#imagen').removeClass('imagen_cortina50').addClass('imagen_cortina100');
            $('#encuadre_personalizado').addClass('hidden');
            $(this).css('width', '100%');
            $('#imagen').css({'left': '0', 'top': '0', 'min-width': '100%', 'min-height': '100%'});
        } else {
            var value = 95 + '%';
            $('#imagen').addClass('imagen_cortina50 draggable resizable').removeClass('imagen_cortina100');
            $('#encuadre_personalizado').removeClass('hidden').css({'width': '25%', 'display': 'inline-flex', 'padding': 0});
            $('#imagen').css({'width': value, 'height': value, 'min-width': 0, 'min-height': 0});
            $(this).css({'display': 'inline-flex', 'padding': 0, 'width': '60%'});

            var imgSrc = $('.div_imagen_cortina').css('background-size');
            var widthImage = imgSrc.split(" ")[0].replace('%', '');
            var heightImage = imgSrc.split(" ")[1].replace('%', '');

            $('#imagen').resizable({
                handles: 'n,e,s,w,ne,se,sw,nw',
                containment: '#cortina'
            });

            $('#imagen').parent().css('width', '100%').css('height', '100%');

            //$('#imagen').css('width', widthImage + '% ').css('height', heightImage + '%').css('min-width', widthImage + '% ').css('min-height', heightImage + '%');

            $('.draggable').draggable({
                containment: '#cortina',
                drag: function () {
                    var thisPos = $(this).position();
                    var parentPos = $(this).parent().position();
                    var x = thisPos.left - parentPos.left;
                    var y = thisPos.top - parentPos.top;
                    var anchoCortina = $('#ancho_cortina').val();
                    var altoCortina = $('#alto_cortina').val();

                    calculateImagePosition(x, y, anchoCortina, altoCortina);

                }
            });
        }
    }

    function alertErrorMessage(message, title) {
        toastr.error(message, title);
    }

    function changeTexto() {
        $('#div_texto_cortina').find('span').html($(this).val());
    }

    function changeTipografia(e) {
        e.preventDefault();
        var font = $(this).attr('font');
        $('#selectFontModal').removeClass().addClass(font).addClass('dropdown-toggle form-control');
        $('#div_texto_cortina').removeClass().addClass(font).addClass('div_texto_cortina draggable ui-draggable-handle');
    }

    function submitEmail(e) {
        e.preventDefault();
        console.log("EMVIAR MAIL...");
        console.log($('.editLiveForm').serialize());
        $.ajax({
            url: "mod_estampacion/ajax/sendEmail.php",
            type: "post",
            data: $('.editLiveForm').serialize(),
            success: function (response) {
                console.log(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("Error ajax en submitFormNewDni");
                console.log(jqXHR, textStatus, errorThrown);
            }

        });
    }

    function clickSelectFont(e) {
        e.preventDefault();
        var font = $(this).attr('font');
        $('.fuenteTexto').val(font);
        $('.preview_font').removeClass().addClass(font).addClass('preview_font');
    }

    function changeColorModal() {
        $('.spantextoCortina').css('color', $(this).val());
    }

    function changeTamanoTextoModal(e) {
        e.preventDefault();
        $('.spantextoCortina').css('font-size', $(this).val() + 'px');

    }

    function changeEncuadrePersonalizado(e) {
        e.preventDefault();
        var value = $(this).val();
        value = value + '%';
        $('#imagen').css({'width': value, 'height': value});
    }

    $(document).on('click', '.btn_load_content', loadContent);
    $(document).on('click', '.paginate_image_gallery', pagination);
    $(document).on('click', '.paginate_image_admin', pagination_admin);
    $(document).on('submit', '#medidas_cortina', modalImage);
    $(document).on('submit', '#buscador_imagenes', searchImages);

    $(document).on('change keypress keyup', '.ancho_cortina, .alto_cortina', editLive);
    $(document).on('change', '.encuadre', changeEncuadre);
    $(document).on('change keypress keyup', '.textoCortina', changeTexto);
    $(document).on('click', '.changeFont', changeTipografia);
    $(document).on('click', '.btn-enviar', submitEmail);
    $(document).on('change', '#categoria_busqueda', function () {
        $('#buscador_imagenes').submit();
    });

    $(document).on('click', '.li_selectFont', clickSelectFont);

    $(document).on('change', '.colorpicker', changeColorPicker);

    $(document).on('change click', '#colorTextoModal', changeColorModal);
    $(document).on('change click', '#tamanoTextoModal', changeTamanoTextoModal);
    $(document).on('change', '.encuadre_personalizado', changeEncuadrePersonalizado);

    //function filtersImage(){
    //   fabric.Object.prototype.transparentCorners = false;
    //   var $ = function(id){return document.getElementById(id)};
    //
    //   function applyFilter(index, filter) {
    //      var obj = canvas.getActiveObject();
    //      obj.filters[index] = filter;
    //      obj.applyFilters(canvas.renderAll.bind(canvas));
    //   }
    //
    //   function applyFilterValue(index, prop, value) {
    //      var obj = canvas.getActiveObject();
    //      if (obj.filters[index]) {
    //         obj.filters[index][prop] = value;
    //         obj.applyFilters(canvas.renderAll.bind(canvas));
    //      }
    //   }
    //
    //   fabric.Object.prototype.padding = 5;
    //   fabric.Object.prototype.transparentCorners = false;
    //
    //   var canvas = this.__canvas = new fabric.Canvas('imagen'),
    //       f = fabric.Image.filters;
    //
    //   fabric.Image.fromURL('../lib/bg.png', function(img) {
    //      canvas.backgroundImage = img;
    //      canvas.backgroundImage.width = 600;
    //      canvas.backgroundImage.height = 600;
    //   });
    //
    //
    //   canvas.on({
    //      'object:selected': function() {
    //         fabric.util.toArray(document.getElementsByTagName('input'))
    //             .forEach(function(el){ el.disabled = false; })
    //
    //         var filters = ['grayscale', 'invert', 'remove-white', 'sepia', 'sepia2',
    //            'brightness', 'contrast', 'saturate', 'noise', 'gradient-transparency', 'pixelate',
    //            'blur', 'sharpen', 'emboss', 'tint', 'multiply', 'blend'];
    //
    //         for (var i = 0; i < filters.length; i++) {
    //            $(filters[i]).checked = !!canvas.getActiveObject().filters[i];
    //         }
    //      },
    //      'selection:cleared': function() {
    //         fabric.util.toArray(document.getElementsByTagName('input'))
    //             .forEach(function(el){ el.disabled = true; })
    //      }
    //   });
    //
    //   fabric.Image.fromURL('../assets/printio.png', function(img) {
    //      var oImg = img.set({ left: 50, top: 100, angle: -15 }).scale(0.9);
    //      canvas.add(oImg).renderAll();
    //      canvas.setActiveObject(oImg);
    //   });
    //   var indexF;
    //   $('grayscale').onclick = function() {
    //      applyFilter(0, this.checked && new f.Grayscale());
    //   };
    //   $('invert').onclick = function() {
    //      applyFilter(1, this.checked && new f.Invert());
    //   };
    //   $('remove-white').onclick = function () {
    //      applyFilter(2, this.checked && new f.RemoveWhite({
    //             threshold: $('remove-white-threshold').value,
    //             distance: $('remove-white-distance').value
    //          }));
    //   };
    //   $('remove-white-threshold').onchange = function() {
    //      applyFilterValue(2, 'threshold', this.value);
    //   };
    //   $('remove-white-distance').onchange = function() {
    //      applyFilterValue(2, 'distance', this.value);
    //   };
    //   $('sepia').onclick = function() {
    //      applyFilter(3, this.checked && new f.Sepia());
    //   };
    //   $('sepia2').onclick = function() {
    //      applyFilter(4, this.checked && new f.Sepia2());
    //   };
    //   $('brightness').onclick = function () {
    //      applyFilter(5, this.checked && new f.Brightness({
    //             brightness: parseInt($('brightness-value').value, 10)
    //          }));
    //   };
    //   $('brightness-value').onchange = function() {
    //      applyFilterValue(5, 'brightness', parseInt(this.value, 10));
    //   };
    //   $('contrast').onclick = function () {
    //      applyFilter(6, this.checked && new f.Contrast({
    //             contrast: parseInt($('contrast-value').value, 10)
    //          }));
    //   };
    //   $('contrast-value').onchange = function() {
    //      applyFilterValue(6, 'contrast', parseInt(this.value, 10));
    //   };
    //   $('saturate').onclick = function () {
    //      applyFilter(7, this.checked && new f.Saturate({
    //             saturate: parseInt($('saturate-value').value, 10)
    //          }));
    //   };
    //   $('saturate-value').onchange = function() {
    //      applyFilterValue(7, 'saturate', parseInt(this.value, 10));
    //   };
    //   $('noise').onclick = function () {
    //      applyFilter(8, this.checked && new f.Noise({
    //             noise: parseInt($('noise-value').value, 10)
    //          }));
    //   };
    //   $('noise-value').onchange = function() {
    //      applyFilterValue(8, 'noise', parseInt(this.value, 10));
    //   };
    //   $('gradient-transparency').onclick = function () {
    //      applyFilter(9, this.checked && new f.GradientTransparency({
    //             threshold: parseInt($('gradient-transparency-value').value, 10)
    //          }));
    //   };
    //   $('gradient-transparency-value').onchange = function() {
    //      applyFilterValue(9, 'threshold', parseInt(this.value, 10));
    //   };
    //   $('pixelate').onclick = function() {
    //      applyFilter(10, this.checked && new f.Pixelate({
    //             blocksize: parseInt($('pixelate-value').value, 10)
    //          }));
    //   };
    //   $('pixelate-value').onchange = function() {
    //      applyFilterValue(10, 'blocksize', parseInt(this.value, 10));
    //   };
    //   $('blur').onclick = function() {
    //      applyFilter(11, this.checked && new f.Convolute({
    //             matrix: [ 1/9, 1/9, 1/9,
    //                1/9, 1/9, 1/9,
    //                1/9, 1/9, 1/9 ]
    //          }));
    //   };
    //   $('sharpen').onclick = function() {
    //      applyFilter(12, this.checked && new f.Convolute({
    //             matrix: [  0, -1,  0,
    //                -1,  5, -1,
    //                0, -1,  0 ]
    //          }));
    //   };
    //   $('emboss').onclick = function() {
    //      applyFilter(13, this.checked && new f.Convolute({
    //             matrix: [ 1,   1,  1,
    //                1, 0.7, -1,
    //                -1,  -1, -1 ]
    //          }));
    //   };
    //   $('tint').onclick = function() {
    //      applyFilter(14, this.checked && new f.Tint({
    //             color: document.getElementById('tint-color').value,
    //             opacity: parseFloat(document.getElementById('tint-opacity').value)
    //          }));
    //   };
    //   $('tint-color').onchange = function() {
    //      applyFilterValue(14, 'color', this.value);
    //   };
    //   $('tint-opacity').onchange = function() {
    //      applyFilterValue(14, 'opacity', parseFloat(this.value));
    //   };
    //   $('multiply').onclick = function() {
    //      applyFilter(15, this.checked && new f.Multiply({
    //             color: document.getElementById('multiply-color').value
    //          }));
    //   };
    //   $('multiply-color').onchange = function() {
    //      applyFilterValue(15, 'color', this.value);
    //   };
    //
    //   $('blend').onclick= function() {
    //      applyFilter(16, this.checked && new f.Blend({
    //             color: document.getElementById('blend-color').value,
    //             mode: document.getElementById('blend-mode').value
    //          }));
    //   };
    //
    //   $('blend-mode').onchange = function() {
    //      applyFilterValue(16, 'mode', this.value);
    //   };
    //
    //   $('blend-color').onchange = function() {
    //      applyFilterValue(16, 'color', this.value);
    //   };
    //}

});