-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-02-2018 a las 08:10:39
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestraining`
--
CREATE DATABASE IF NOT EXISTS `gestraining` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `gestraining`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trainings`
--

DROP TABLE IF EXISTS `trainings`;
CREATE TABLE `trainings` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_bin NOT NULL,
  `description` varchar(255) COLLATE utf8_bin NOT NULL,
  `long_description` longtext COLLATE utf8_bin NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `image_1` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image_2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image_3` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image_4` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image_5` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `trainings`
--

INSERT INTO `trainings` (`id`, `name`, `description`, `long_description`, `id_user`, `status`, `image_1`, `image_2`, `image_3`, `image_4`, `image_5`) VALUES
(1, 'Prueba2018', 'Entrenamiento de prueba2018', 'Consiste en bla bla bla', 1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_bin NOT NULL,
  `surname` varchar(40) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `pass` varchar(255) COLLATE utf8_bin NOT NULL,
  `is_player` int(1) NOT NULL DEFAULT '0',
  `is_admin` int(1) NOT NULL DEFAULT '0',
  `registration_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `pass`, `is_player`, `is_admin`, `registration_date`, `status`) VALUES
(1, 'Iker', 'Zapata', 'ikerzaphoz@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0, 1, '2018-01-31 10:03:12', 1),
(2, 'Nombre', 'Apellidos', 'a@a.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 0, 0, '2018-01-31 12:29:39', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
