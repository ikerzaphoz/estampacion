<div id="estampacion_content_form">
    <form class="text-center medidas_cortina" id="medidas_cortina" name="medidas_cortina" method="post">
        <div class="form-group col-md-12">
            <h5>Datos cortina / imagen</h5>
            <div class="form-group col-md-6">
                <label for="ancho">Ancho</label><input type="text" required class="form-control mt-5" id="ancho" name="ancho" placeholder="Ancho cortina">
                <label for="alto">Alto</label><input type="text" required class="form-control mt-5" id="alto" name="alto" placeholder="Alto cortina">
            </div>
            <div class="form-group col-md-6">
                <label for="encuadre">Encuadre</label>
                <select id="encuadre" name="encuadre" class="selectpicker encuadre form-control mt-5">
                    <option disabled>Seleccionar encuadre</option>
                    <option value="100">100%</option>
                    <option value="50">Personalizado</option>
                </select>
                <label for="vistaImagen">Vista</label>
                <select id="vistaImagen" name="vistaCortina" class="selectpicker vistaCortina form-control mt-5">
                    <option disabled>Seleccionar vista</option>
                    <option value="interior">Interior</option>
                    <option value="exterior">Exterior</option>
                </select>
            </div>
        </div>
        <div class="form-group col-md-12">
            <h5>Datos texto</h5>
            <div class="col-md-6">
                <input type="text" class="form-control mt-5" id="textoCortina" name="textoCortina" placeholder="Texto cortina">
            </div>
            <div class="col-md-6">
                <button class="dropdown-toggle form-control mt-5" id="selectFont" data-toggle="dropdown"><span class="font_fantasy preview_font">Fuente</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu ml-15" role="menu" aria-labelledby="selectFont">
                    <li class="fuente_texto" role="presentation"><a font="font_fantasy" class="font_fantasy li_selectFont" role="menuitem" tabindex="-1" href="#">Fantasy</a></li>
                    <li class="fuente_texto" role="presentation"><a font="font_cursive" class="font_cursive li_selectFont" role="menuitem" tabindex="-1" href="#">Cursive</a></li>
                    <li class="fuente_texto" role="presentation"><a font="font_mono" class="font_mono li_selectFont" role="menuitem" tabindex="-1" href="#">Monospace</a></li>
                    <li class="fuente_texto" role="presentation"><a font="font_sans" class="font_sans li_selectFont" role="menuitem" tabindex="-1" href="#">Sans-serif</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <label for="colorTexto">Color texto</label>
                <input style="background-color: #000000" type="text" class="form-control mt-5 colorpicker" id="colorTexto" value="#000000" placeholder="Color texto" name="colorTexto">
            </div>
            <div class="col-md-6">
                <label for="tamanoTexto">Tamaño texto</label>
                <input type="number" min="12" max="36" class="form-control mt-5" id="tamanoTexto" value="12" name="tamanoTexto">
            </div>
        </div>
        <div class="form-group">
            <input type="hidden" class="ref_image" name="ref_image" id="ref_image">
            <input type="hidden" class="src_image" name="src_image" id="src_image">
            <input type="hidden" class="fuenteTexto" name="fuenteTexto" id="fuenteTexto">
            <button type="submit" class="btn btn-success btn_submitForm_medidas_cortinas">Visualizar</button>
        </div>
    </form>
</div>