<?php

$page = 'index';
$classHidden = "";
$classLang = "";

include_once '../config.php';
include root.'mod_head/index.php';
include root.'mod_header/index.php';
include root.'mod_sliders/index.php';
include root.'mod_title_page/index.php'

?>

<section id="index_content">

    <div class="col-md-4">
        <span class="title_option_index">Estampación digital</span>
        <p class="text_option_index">Ponemos a su disposición un banco de imágenes para que elija la que usted prefiera para decorar su casa o su local. Nosotros imprimimos la imagen sobre un tejido Screenflex FV 4301 color 1641.</p>
        <a href="<?=path_web?>estampacion.php#title_page"><span class="btn btn-info">Ir a estampación digital</span></a>
    </div>
    <div class="col-md-4">
        <span class="title_option_index">Impresión digital</span>
        <p class="text_option_index">Usted nos proporciona una imagen y nosotros la imprimimos en la cortina. Opción adecuada para fotografías o logos sobre fondo blanco o colores claros.</p>
        <a href="<?=path_web?>impresion.php?type=digital#title_page"><span class="btn btn-info">Ir a impresión digital</span></a>
    </div>
    <div class="col-md-4">
        <span class="title_option_index">Impresión por serigrafía</span>
        <p class="text_option_index">Usted nos proporciona una imagen y nosotros la serigrafíamos en la cortina. Opción adecuada para fotografías o logos con fondos negros u oscuros.</p>
        <a href="<?=path_web?>impresion.php?type=serigrafia#title_page"><span class="btn btn-info">Ir a impresion serigrafia</span></a>
    </div>

</section>

<?php

include root.'mod_footer/index.php';

?>
