<?php

require '../../config.php';

$texto_busqueda = $_POST['texto_busqueda'];

$image = new Image();
$array_name_images = array();
$optSearch = "code";
if(isset($_POST['optSearch'])):
    $optSearch = $_POST['optSearch'];
endif;
$categoria_busqueda = "";
if(isset($_POST['categoria_busqueda'])):
    $categoria_busqueda = $_POST['categoria_busqueda'];
endif;
if(empty($texto_busqueda) AND empty($categoria_busqueda)):
    $array_name_images = $image->getAllImages();
else:
    $array_name_images = $image->searchImage($texto_busqueda,$optSearch,0,8,'',$categoria_busqueda);
    if(empty($array_name_images)):
        ?>
        <script>
            function alertErrorMessage(message, title){
                toastr.error(message, title);
            }
        </script>
        <?php if($optSearch == "code"): ?>
            <script>alertErrorMessage('No se encuentran resultados', 'Búsqueda código');</script>
        <?php else: ?>
            <script>alertErrorMessage('No se encuentran resultados', 'Búsqueda etiquetas');</script>
        <?php endif; ?>
        <?php
    endif;
endif;

$rowsPerPage = 8;
$pageNum = 0;
$offset = 0;
$total_images = $image->countTotal();
$total_paginas = ceil($total_images / $rowsPerPage);

$i=0;
?><ul id="aspect"><?php
foreach($array_name_images as $image): ?>
    <?php if($i < 8): ?>
        <li class="col-md-12"><a href="#"><img data-toggle="tooltip" data-placemente="top" placeholder="<?=$image['ref_image']?>" title="<?=$image['ref_image']?>" ref-image="<?=$image['ref_image']?>" class="col-md-12 image_galery lazy" data-original="<?=$image['url_image']?>"/></a></li>
    <?php endif;?>
    <?php $i++;?>
<?php endforeach;
?></ul><?php


if ($total_paginas > 1) {
    echo '<div class="pagination text-center">';
    echo '<ul>';
    for ($i=1;$i<=$total_paginas;$i++) {
        if ($pageNum == $i)
            echo '<li class="active"><a>'.$i.'</a></li>';
        else
            echo '<li><a class="paginate paginate_image_gallery" data="'.$i.'">'.$i.'</a></li>';
    }
    if ($pageNum != $total_paginas)
        echo '<li><a class="paginate paginate_image_gallery" data="2"><i class="glyphicon glyphicon-arrow-right"></i></a></li>';
    echo '</ul>';
    echo '</div>';
    }

?>