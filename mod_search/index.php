<div id="estampacion_content_buscador">
    <form class="text-center buscador_imagenes form-inline" id="buscador_imagenes" name="buscador_imagenes" method="post">
        <input type="search" class="form-control" id="texto_busqueda" name="texto_busqueda" placeholder="Introduce búsqueda. Ej: Leones"/>
        <?php
        if(!empty($categories)):?>
            <select title="" class="form-control selectpicker" id="categoria_busqueda" name="categoria_busqueda">
                <option value="">Todas</option>
                <?php
                foreach($categories as $category): ?>
                    <option value="<?=$category['category']?>"><?=$category['category']?></option>
                <?php endforeach;
                ?>
            </select>
            <?php
        endif;
        ?>
<!--        <label class="radio-inline">-->
<!--            <input type="radio" class="optSearch" name="optSearch" checked value="code">Buscar por código-->
<!--        </label>-->
<!--        <label class="radio-inline">-->
            <input type="hidden" checked class="optSearch" name="optSearch" value="tags">
<!--        </label>-->
    </form>
</div>