<div id="content_login" class="row">
    <form id="form_login" class="form_login text-center" method="post" action="<?=path_web?>mod_login/action/ini_log.php">
        <span class="title_form_login">
            <span>Acceso clientes</span>
            <i class="glyphicon glyphicon-user"></i>
        </span>
        <input name="user_login" data-toggle="tooltip" data-placement="top" placeholder="Email / Nombre usuario" title="Email / Nombre usuario" type="text" id="user_login" class="form-control user_login">
        <input name="pass_login" data-toggle="tooltip" data-placemente="top" placeholder="Contraseña" title="Contraseña" type="password" id="pass_login" class="form-control pass_login">
        <input type="submit" id="submit_login" class="btn btn-info center-block" value="Iniciar sesión">
    </form>
</div>