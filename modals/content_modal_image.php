<?php
require '../config.php';


$imagen = $_POST['src_image'];

//$porcentajeAncho = round($imagen['max_width'] / $_POST['ancho'],2) * 100;
//$porcentajeAlto = round($imagen['max_height'] / $_POST['alto'],2) * 100;

$porcentajeAncho = 100;
$porcentajeAlto = 100;

$encuadre = $_POST['encuadre'];

$vistaCortina = $_POST['vistaCortina'];

$textoCortina = "";

$textoCortina = $_POST['textoCortina'];

$fuenteTexto = $_POST['fuenteTexto'];

$tamanoTexto = $_POST['tamanoTexto'];

$colorTexto = $_POST['colorTexto'];
?>
<form name="editLiveForm" id="editLiveForm" class="editLiveForm">
    <input type="hidden" class="image_canvas_src" value="<?=$_POST['src_image']?>">
    <div class="form-group infoCortina">
        <h5>Datos cortina</h5>
        <div class="form-group col-md-3">
            <label for="ancho_cortina">Ancho:</label>
            <input size="5" type="text" name="ancho_cortina" class="form-control ancho_cortina" id="ancho_cortina" value="<?= $_POST['ancho'] ?>">
            <label for="alto_cortina">Alto:</label>
            <input size="5" type="text" name="alto_cortina" class="form-control alto_cortina" id="alto_cortina" value="<?= $_POST['alto'] ?>">
        </div>
        <div class="form-group col-md-3">
            <label for="encuadre">Encuadre:</label>
            <div class="select_encuadre text-center">
                <select id="encuadre" name="encuadre" class="form-control encuadre">
                    <option disabled>Seleccionar encuadre</option>
                    <option <?php if ($encuadre == 100): echo 'selected';
endif; ?> value="100">100%</option>
                    <option <?php if ($encuadre == 50): echo 'selected';
endif; ?> value="50">Personalizado</option>
                </select>
                <select name="encuadre_personalizado" id="encuadre_personalizado" class="hidden form-control encuadre_personalizado">
                    <option value="95">95%</option>
                    <option value="90">90%</option>
                    <option value="85">85%</option>
                    <option value="80">80%</option>
                </select>
            </div>
            <label for="vistaImagen">Vista:</label>
            <select id="vistaImagen" name="vistaCortina" class="form-control vistaCortina">
                <option disabled>Seleccionar vista</option>
                <option <?php if ($vistaCortina == 'interior'): echo 'selected';
endif; ?> value="interior">Interior</option>
                <option <?php if ($vistaCortina == 'exterior'): echo 'selected';
endif; ?> value="exterior">Exterior</option>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="textoCortina">Texto:</label>
            <input <?php if (!empty($textoCortina)): echo "value='$textoCortina'";
endif; ?> type="text" class="form-control textoCortina" id="textoCortina" name="textoCortina" placeholder="Texto cortina">
            <label for="selectFontModal">Fuente:</label>
            <div class="form-group dropdown">
                <button class="dropdown-toggle form-control <?= $fuenteTexto ?>" id="selectFontModal" data-toggle="dropdown">Vista previa
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu" aria-labelledby="selectFont">
                    <li class="fuente_texto" role="presentation"><a font="font_fantasy" class="font_fantasy changeFont" role="menuitem" tabindex="-1" href="#">Fantasy</a></li>
                    <li class="fuente_texto" role="presentation"><a font="font_cursive" class="font_cursive changeFont" role="menuitem" tabindex="-1" href="#">Cursive</a></li>
                    <li class="fuente_texto" role="presentation"><a font="font_mono" class="font_mono changeFont" role="menuitem" tabindex="-1" href="#">Monospace</a></li>
                    <li class="fuente_texto" role="presentation"><a font="font_sans" class="font_sans changeFont" role="menuitem" tabindex="-1" href="#">Sans-serif</a></li>
                </ul>
            </div>
        </div>
        <div class="form-group col-md-3">
            <label for="colorTextoModal">Color texto</label>
            <input style="background-color: <?= $colorTexto ?>" type="text" class="form-control colorpicker" id="colorTextoModal" placeholder="Color texto" value="<?= $colorTexto ?>" name="colorTextoModal">
            <label for="tamanoTextoModal">Tamaño texto</label>
            <input type="number" min="12" max="36" class="form-control" id="tamanoTextoModal" value="<?= $tamanoTexto ?>" name="tamanoTextoModal">
        </div>
    </div>
    <input type="hidden" name="ref_image" value="<?= $_POST['ref_image'] ?>">
</form>

<div style="display: inline-block; margin-left: 10px">
    <button id="drawing-mode" class="btn btn-info btn-sm">Desactivar modo dibujo</button><br>
    <button id="clear-canvas" class="btn btn-info btn-sm">Borrar</button><br>

    <div id="drawing-mode-options">
        <label for="drawing-mode-selector">Herramienta:</label>
        <select id="drawing-mode-selector">
            <option>Lapiz</option>
            <option>Spray</option>
            <option>Linea horizontal</option>
            <option>Linea vertical</option>
        </select><br>

        <label for="drawing-line-width">Line width:</label>
        <span class="info">30</span><input type="range" value="30" min="0" max="150" id="drawing-line-width"><br>

        <label for="drawing-color">Line color:</label>
        <input type="text" class="colorpicker" value="#000000" id="drawing-color"><br>

    </div>
</div>

<div id="cortina" class="cortina">
    <canvas id="canvas">
        <div id="div_imagen_cortina" class="div_imagen_cortina" style="background-size: <?= $porcentajeAncho ?>% <?= $porcentajeAlto ?>%;">
            <div id="imagen" class="<?php if ($_POST['encuadre'] == '100'): echo "imagen_cortina100";
else: echo "draggable resizable imagen_cortina50";
endif; ?>" style="background-image: url(<?= $_POST['src_image'] ?>);"></div>
        </div>
    </canvas>
    <div id="div_texto_cortina" class="div_texto_cortina draggable <?= $fuenteTexto ?>">
                <span class="spantextoCortina" style="font-size: <?= $tamanoTexto ?>px; color: <?= $colorTexto ?>;"><?= $textoCortina ?></span>
            </div>
</div>