<div class="modal fade text-center" id="modal-basic" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Title</h3>
            </div>
            <div class="modal-body">
                <div class="content_modal_body">
                    <p>Contenido modal</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="text-right btn btn-info btn-enviar hidden">Enviar</button>
                <button type="button" class="text-left btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>