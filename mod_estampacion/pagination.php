<?php
require_once '../config.php';

$image = new Image();
$total_images = $image->countTotal();

if ($total_images > 0) {
    $rowsPerPage = 8;
    $pageNum = 1;
    if(isset($_POST['page'])) {
        sleep(1);
        $pageNum = $_POST['page'];
    }
    $offset = ($pageNum - 1) * $rowsPerPage;
    $total_paginas = ceil($total_images / $rowsPerPage);
    $texto_busqueda = "";
    if(isset($_POST['texto_busqueda'])):
        $texto_busqueda = $_POST['texto_busqueda'];
    endif;
    $optSearch = "tags";
    if(isset($_POST['optSearch'])):
        $optSearch = $_POST['optSearch'];
    endif;

    $categoria_busqueda = "";
    if(isset($_POST['categoria_busqueda'])):
        $categoria_busqueda = $_POST['categoria_busqueda'];
    endif;

    //Pendiente añadir pagindo teniendo en cuenta la categoría búscada
    $array_images = $image->searchImage($texto_busqueda, $optSearch, $offset, $rowsPerPage, $pageNum);

    ?>
    <ul id="aspect">

    <?php

    foreach($array_images as $imageItem):?>

        <li class="col-md-12"><a href="#"><img data-toggle="tooltip" data-placemente="top" placeholder="<?=$imageItem['ref_image']?>" title="<?=$imageItem['ref_image']?>" ref-image="<?=$imageItem['ref_image']?>" class="col-md-12 image_galery lazy" data-original="<?=$imageItem['url_image']?>"/></a></li>

    <?php endforeach; ?>

    </ul>

    <?php

    if ($total_paginas > 1) {
        echo '<div class="pagination text-center">';
        echo '<ul>';
        if ($pageNum != 1)
            echo '<li><a class="paginate paginate_image_gallery" data="'.($pageNum-1).'"><i class="glyphicon glyphicon-arrow-left"></i></a></li>';
        for ($i=1;$i<=$total_paginas;$i++) {
            if ($pageNum == $i)
                echo '<li class="active"><a>'.$i.'</a></li>';
            else
                echo '<li><a class="paginate paginate_image_gallery" data="'.$i.'">'.$i.'</a></li>';
        }
        if ($pageNum != $total_paginas)
            echo '<li><a class="paginate paginate_image_gallery" data="'.($pageNum+1).'"><i class="glyphicon glyphicon-arrow-right"></i></a></li>';
        echo '</ul>';
        echo '</div>';
    }

}