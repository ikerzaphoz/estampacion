<div class="container">
    <header id="header" class="row">
        <img id="img_header" src="<?=path_web?>/images/cabecera-380.png">
        <nav id="nav_menu">
            <ul class="list-unstyled list-inline">
                <li class="<?=$classHidden?><?php if($page == 'index'): echo ' active'; endif; ?>"><a href="<?=path_web?>index.php#title_page"><?=lang_menu_inicio?></a></li>
                <li class="<?=$classHidden?><?php if($page == 'estampacion'): echo ' active'; endif; ?>"><a href="<?=path_web?>estampacion.php#title_page">ESTAMPACIÓN</a></li>
                <li class="<?=$classHidden?><?php if($page == 'impresion'): echo ' active'; endif; ?>"><a href="<?=path_web?>impresion.php#title_page">IMPRESIÓN</a></li>
                <div class="li_lang <?=$classLang?>">
                    <li class="no_hover"><a href="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']?>?lang=es"><div class="lazy icon-flag flag-es"></div></a></li>
                    <li class="no_hover"><a href="http://<?=$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']?>?lang=pt"><div class="lazy icon-flag flag-pt"></div></a></li>
                </div>
            </ul>
        </nav>
    </header>