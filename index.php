<?php

$page = 'login';
$classHidden = "hidden";
$classLang = 'onlyLang';

include_once 'config.php';
include 'mod_head/index.php';
include 'mod_header/index.php';
include 'mod_sliders/index.php';
include 'mod_login/index.php';
include 'mod_footer/index.php';