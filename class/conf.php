<?php

Class Conf{

   private $_domain;
   private $_userdb;
   private $_passdb;
   private $_hostdb;
   private $_db;

   //Datos con la conexión de a la base de datos
   public function __construct(){
      $this->_domain='';
      $this->_userdb='root';
      $this->_passdb='';
      $this->_hostdb='localhost';
      $this->_db='estampaciondigital';
   }

   public function getUserDB(){
      $var=$this->_userdb;
      return $var;
   }

   public function getHostDB(){
      $var=$this->_hostdb;
      return $var;
   }

   public function getPassDB(){
      $var=$this->_passdb;
      return $var;
   }

   public function getDB(){
      $var=$this->_db;
      return $var;
   }

}

?>