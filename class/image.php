<?php

Class Image{

    private $id;
    private $ref_image;
    private $url_image;
    private $format_image;

    public function __construct($id=null, $ref_image=null, $url_image=null, $format_image=null)
    {
        $this->id = $id;
        $this->ref_image = $ref_image;
        $this->url_image = $url_image;
        $this->format_image = $format_image;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getRefImage()
    {
        return $this->ref_image;
    }

    public function setRefImage($ref_image)
    {
        $this->ref_image = $ref_image;
    }

    public function getUrlImage()
    {
        return $this->url_image;
    }

    public function setUrlImage($url_image)
    {
        $this->url_image = $url_image;
    }

    public function getFormatImage()
    {
        return $this->format_image;
    }

    public function setFormatImage($format_image)
    {
        $this->format_image = $format_image;
    }

    public function getAllImages(){
        $bd = new Db();
        $sql = "SELECT * FROM images";
        $result = $bd->obtener_consultas($sql, 'ref_image');
        return $result;
    }

    public function getImage($image){
        $bd = new Db();
        $sql = "SELECT * FROM images WHERE ref_image = '$image'";
        $result = $bd->obtener_consultas($sql);
        return $result;
    }

    public function checkRefImage($image){
        $bd = new Db();
        $sql = "SELECT * FROM images WHERE ref_image = '$image'";
        $affected = $bd->ejecutarReturnAffected($sql);
        return $affected;
    }

    public function insertImage($ref_image, $url_image, $format_image, $ppp, $width, $max_width, $min_width, $max_height, $min_height, $height){
        $bd = new Db();
        $affected = $this->checkRefImage($ref_image);
        $ref_image_insert = 0;
        if($affected <= 0):
            $sql = "INSERT INTO images (ref_image, url_image, format_image, ppp, width, max_width, min_width, max_height, min_height, height) VALUES ($ref_image, '$url_image', '$format_image', $ppp, $width, $max_width, $min_width, $max_height, $min_height, $height)";
            $bd->ejecutar($sql);
            $ref_image_insert = $ref_image;
        endif;
        return $ref_image_insert;
    }

    public function countTotal(){
        $bd = new Db();
        $sql = "SELECT id FROM images";
        $affected = $bd->ejecutarReturnAffected($sql);
        return $affected;
    }

    public function searchImage($search = NULL, $optionSearch = NULL, $offset = NULL, $rowsPerPage = NULL, $paginado = NULL, $categoria_busqueda = NULL){
        $bd = new Db();
        $result = [];
        $sql = "SELECT * FROM images";
        $sql_aux = "SELECT * FROM images";
        if(!empty($search)):
            if(empty($categoria_busqueda)):
                if($optionSearch == "code"):
                    $sql .= " WHERE ref_image = '$search'";
                    $sql_aux .= " WHERE ref_image <> '$search'";
                else:
                    $sql .= " WHERE tags LIKE '%$search%'";
                    $sql_aux .= " WHERE tags IS NULL OR tags NOT LIKE '%$search%'";
                endif;
            else:
                $sql .= " WHERE category = '$categoria_busqueda' AND tags LIKE '%$search%'";
                $sql_aux .= " WHERE category <> '$categoria_busqueda' OR category IS NULL AND tags IS NULL OR tags NOT LIKE '%$search%'";
            endif;
        endif;
        if(empty($search) AND !empty($categoria_busqueda)):
            $sql .= " WHERE category = '$categoria_busqueda'";
            $sql_aux .= " WHERE category <> '$categoria_busqueda' OR category IS NULL";
        endif;
        $sql_order = " ORDER BY ref_image ASC LIMIT $offset, $rowsPerPage";
        foreach($bd->obtener_consultas($sql.$sql_order, 'ref_image') as $item):
            if(!empty($result)):
                $result_buscar = in_array($item['ref_image'],$result);
                if($result_buscar != 1):
                    $result[$item['ref_image']] = $item;
                endif;
            else:
                $result[$item['ref_image']] = $item;
            endif;
        endforeach;
        if(!empty($search)):
            if($offset == 0):
                if(isset($paginado)):
                    $rowsPerPage = $rowsPerPage - count($result);
                endif;

            elseif($offset > 0):
                if(isset($paginado)):
                    if($optionSearch == "code"):
                        $offset = $offset - $bd->ejecutarReturnAffected($sql);
                    else:
                        if(count($result) > 0):
                        $offset = 0;
                        else:
                            $offset = $offset - $bd->ejecutarReturnAffected($sql);
                        endif;
                        $rowsPerPage = $rowsPerPage - count($result);
                    endif;
                endif;
            endif;
        endif;
        $sql_aux.=" ORDER BY ref_image ASC LIMIT $offset, $rowsPerPage";
        foreach($bd->obtener_consultas($sql_aux, 'ref_image') as $item):
            if(!empty($result)):
                $result_buscar = in_array($item['ref_image'],$result);
                if($result_buscar != 1):
                    $result[$item['ref_image']] = $item;
                endif;
            else:
                $result[$item['ref_image']] = $item;
            endif;
        endforeach;
        return $result;
    }

    function addTag($ref_image, $value){
        $bd = new Db();
        $sql = "UPDATE images set tags = '$value' WHERE id = '$ref_image'";
        return $bd->ejecutarReturnAffected($sql);
    }

    function addCategory($ref_image, $value){
        $bd = new Db();
        $sql = "UPDATE images set category = '$value' WHERE id = '$ref_image'";
        return $bd->ejecutarReturnAffected($sql);
    }

    function removeTag($ref_image, $value){
        $bd = new Db();
        $sql = "UPDATE images set tags = '$value' WHERE id = '$ref_image'";
        return $bd->ejecutarReturnAffected($sql);
    }

    function removeCategory($ref_image, $value){
        $bd = new Db();
        $sql = "UPDATE images set category = '$value' WHERE id = '$ref_image'";
        return $bd->ejecutarReturnAffected($sql);
    }

    function getCategories(){
        $bd = new Db();
        $sql = "SELECT DISTINCT (category) FROM images WHERE category <> ''";
        $array_categories = $bd->obtener_consultas($sql);
        $array_aux = array();
        foreach($array_categories as $category):
            if(strpos($category['category'], ',') !== false):
            else:
                array_push($array_aux, $category);
            endif;
        endforeach;
        return $array_aux;
    }

    function updateRef($ref_image, $value){
        $bd = new Db();
        $sql = "SELECT ref_image FROM images WHERE ref_image = '$value'";
        $affected = $bd->ejecutarReturnAffected($sql);
        if($affected > 0):
            $array_error = array('error' => '1', 'affected' => '-1');
        else:
            $sql = "UPDATE images set ref_image = '$value' WHERE id = '$ref_image'";
            $affected = $bd->ejecutarReturnAffected($sql);
            if($affected == 1):
                $sql = "SELECT id FROM images WHERE ref_image = '$value'";
                $data = $bd->obtener_consultas($sql);
                $array_error = array('error' => '0', 'affected' => '1', 'id_image' => $data[0]);
            else:
                $array_error = array('error' => '1', 'affected' => '-1');
            endif;
        endif;
        return $array_error;

    }

    public function renameFile($old_name, $new_name){
        $result = "";
        $dir_old = root.'images/galery/'.$old_name.'.jpg';
        $dir_new = root.'images/galery/'.$new_name.'.jpg';
        if(file_exists(root.'images/galery/'.$old_name.'.jpg')):
            $result = "1";
            rename($dir_old, $dir_new);
        else:
            $result = "0";
        endif;

        return $result;
    }

    public function getSelectCategory(){
        $category_images = $this->getCategories();
    }

}

?>