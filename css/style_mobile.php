@media (min-width: 320px) AND (max-width: 567px) {

//HEADER
#header {
height: 15%;
margin:0 !important;

#img_header{
top: 25%;
width: 90%;
left: 5%;
position: absolute;
}

#nav_menu{
margin-top: 80px;
display: inline-block;
width: 100%;
margin-bottom: 0 !important;
min-height: 0 !important;

li.active{
background-color: #b4162c;
border-radius: 4px;
color: #FFFFFF;
:hover{
color: #FFFFFF;
}
}

li:hover{
background-color: #b4162c;
border-radius: 4px;
cursor: pointer;
color: #000000;
}

li a{
text-decoration: none;
color: inherit;
font-size: 12px;
}
}

#header_lang{
margin-top: 90px;
display: inline-block;
float: right;
width: 100px;
height: 20px;
text-align: right;
}

.icon_flag{
width: 20px;
height: 20px;
display: inline-block;
}

.flag-es, .flag-pt{
height:15px;
width:20px;
background-position: 50%;
background-repeat: no-repeat;
cursor: pointer;
}

.flag-es{
background-image: url('../images/flags/flags/flat/24/Spain.png');
}

.flag-pt{
background-image: url('../images/flags/flags/flat/24/Portugal.png');
}
}
//END HEADER

//SLIDERS
#sliders{
height: 30%;
#myCarousel{
height: 100%;
-webkit-box-shadow: 0 0 20px 0 rgba(0,0,0,0.75);
-moz-box-shadow: 0 0 20px 0 rgba(0,0,0,0.75);
box-shadow: 0 0 20px 0 rgba(0,0,0,0.75);
.carousel-inner{
height: 100%;
.item{
height: 100%;
img{
height: 100%;
}
}
}
}
}

//END SLIDERS

//LOGIN
#content_login{
height:30%;
}
#form_login{
padding-top: 15%;
width: 80%;
left: 10%;
position: absolute;
input {
.br-4;
text-align: center;
}
.user_login{
margin-bottom: 2%;
}
#pass_login{
margin-bottom: 2%;
}
}
//END LOGIN

//TITLE PAGE
#title_page{
background-color: #b4162c;
height: 10%;
.text_title_page{
color: #ffffff;
padding-bottom: 15px;
padding-top: 15px;
font-size: 18px;
font-family: "Marker Felt" !important;
}
}
//END TITLE PAGE

//INDEX
#index_content{
margin: 20px 0 20px 0;
text-align: center;
.title_option_index{
display: none;
}
div{
background-color: #FFFFFF;
border-radius: 4px;
color: #000000;
width: 100%;
text-align: center;
margin-bottom: 5px;
margin-top: 5px;
cursor: pointer;
.btn-info{
margin-bottom: 10px;
}
a{
color: #FFFFFF;
}
}
}
//END INDEX

#footer{
height: 25%;
margin-bottom: 0;
#img_footer{
position: absolute;
left: 0;
bottom: 0;
width: 100%;
}
}

}
