<?php

/* "https://pixabay.com/api/?key=5207830-59d7440da59fbb784b49e98c0&response_group=high_resolution&q=yellow+flower&pretty=true" */

/* https://pixabay.com/api/videos/?key=5207830-59d7440da59fbb784b49e98c0&q=yellow+flowers&pretty=true */

$content = "";

$search = "dog+cat";

try {
    $ch = curl_init();

    if (FALSE === $ch)
        throw new Exception('failed to initialize');

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

    curl_setopt($ch, CURLOPT_URL, "https://pixabay.com/api/?key=5207830-59d7440da59fbb784b49e98c0&response_group=high_resolution&q=" . $search . "&pretty=true&per_page=12&min_width=2000&min_height=2000");
    //curl_setopt($ch, CURLOPT_URL, "https://pixabay.com/api/videos/?key=5207830-59d7440da59fbb784b49e98c0&q=yellow+flowers&pretty=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    $content = curl_exec($ch);

    if (FALSE === $content)
        throw new Exception(curl_error($ch), curl_errno($ch));
} catch (Exception $e) {

    echo "-----ERROR-----<br>";
    var_dump($e);
    echo "<br>-----ERROR-----";

    trigger_error(sprintf(
                    'Curl failed with error #%d: %s', $e->getCode(), $e->getMessage()), E_USER_ERROR);
}

$array_format = $content;
$array_format = json_decode($array_format,true);
$array_images = $array_format['hits'];