<?php

//Mostramos errores
ini_set("display_errors", 1);

//Constantes
define("root", $_SERVER['DOCUMENT_ROOT']."/projects/estampacion/");
define("path_web", "/projects/estampacion/");
define("path_admin", "/projects/estampacion/admin/");
define("path_image_galery", "/projects/estampacion/images/galery/");

//Idioma
include_once 'controller_lang.php';

//Clases
require 'class/conf.php';
require 'class/db.php';
require 'class/image.php';


?>