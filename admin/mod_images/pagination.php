<?php

require_once '../../config.php';
$image = new Image();
$total_images = $image->countTotal();
$texto_busqueda = "";

if ($total_images > 0) {
    $rowsPerPage = 8;
    $pageNum = 1;
    if (isset($_POST['page'])) {
        sleep(1);
        $pageNum = $_POST['page'];
    }
    $offset = ($pageNum - 1) * $rowsPerPage;
    $total_paginas = ceil($total_images / $rowsPerPage);

    $array_images = $image->searchImage($texto_busqueda,'', $offset, $rowsPerPage);

    ?>

    <h4>Editar Imágenes</h4>
    <table class="table table-striped">
        <thead>
        <th>Ref</th>
        <th>Imagen</th>
        <th>Ancho (100pp)</th>
        <th>Alto (100ppp)</th>
        <th>Ancho (300ppp)</th>
        <th>Alto (300ppp)</th>
        <th>Etiquetas</th>
        <th>Categorías</th>
        </thead>
        <tbody class="contentTable">

        <?php

        foreach ($array_images as $imageItem):?>

            <tr id_image="<?= $imageItem['id'] ?>">
                <input type="hidden" name="old_refImage" value="<?=$imageItem['ref_image']?>" class="old_refImage"/>
                <td class="inline_table"><input readonly class="changeRef tokendfield_readonly" type="text" title="" name="ref_image" value="<?=$imageItem['ref_image']?>"/>
                    <i class="activeEdit glyphicon glyphicon-edit btn-lg btn"></i>
                </td>
                <td class="td_preview_image">
                    <a class="image-popup-no-margins" href="<?= $imageItem['url_image'] ?>"
                       title="Etiquetas: <?= $imageItem['tags'] ?>">
                        <img src="<?= $imageItem['url_image'] ?>" class="preview_image">
                    </a>
                </td>
                <td><span><?=$imageItem['min_width']?></span></td>
                <td><span><?=$imageItem['min_height']?></span></td>
                <td><span><?=$imageItem['max_width']?></span></td>
                <td><span><?=$imageItem['max_height']?></span></td>
                <td><input class="tokenfield" size="10" type="text" title="" name="tags" readonly
                           value="<?= $imageItem['tags'] ?>"/></td>
                <td><input class="tokenfield_category" size="10" type="text" title="" name="category"
                           readonly value="<?=$imageItem['category']?>"/></td>
            </tr>

        <?php endforeach; ?>

        </tbody>
    </table>
    <?php
    if ($total_paginas > 1) {
        echo '<div class="pagination text-center">';
        echo '<ul>';
        if ($pageNum != 1)
            echo '<li><a class="paginate paginate_image_admin" data="'.($pageNum-1).'"><i class="glyphicon glyphicon-arrow-left"></i></a></li>';
        for ($i = 1; $i <= $total_paginas; $i++) {
            if ($pageNum == $i)
                echo '<li class="active"><a>' . $i . '</a></li>';
            else
                echo '<li><a class="paginate paginate_image_admin" data="' . $i . '">' . $i . '</a></li>';
        }
        if ($pageNum != $total_paginas)
            echo '<li><a class="paginate paginate_image_admin" data="'.($pageNum+1).'"><i class="glyphicon glyphicon-arrow-right"></i></a></li>';
        echo '</ul>';
        echo '</div>';
    }

}
?>
