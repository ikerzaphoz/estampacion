<?php

require '../../../config.php';

$image = new Image();
$affected = $image->addTag($_POST['id_image'],$_POST['valueTag']);
if($affected == 1):
    $array_error = array('error' => '0', 'message' => 'Etiqueta añadida correctemente');
elseif($affected == 0):
    $array_error = array('error' => '1', 'message' => 'Cambios no encontrados');
else:
    $array_error = array('error' => '2', 'message' => 'Error al modificar etiqueta');
endif;

echo json_encode($array_error);