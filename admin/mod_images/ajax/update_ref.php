<?php

require '../../../config.php';

$image = new Image();
$return = $image->updateRef($_POST['id_image'],$_POST['valueRef']);
if($return['affected'] == 1):
    $rename = $image->renameFile($_POST['ref_old'], $_POST['valueRef']);
    if($rename == 1):
        $array_error = array('archivo_rename' => $rename, 'error' => '0', 'message' => 'Referencia y archivo modificados correctemente');
    else:
        $array_error = array('archivo_rename' => $rename, 'error' => '0', 'message' => 'Referencia modificada correctemente');
    endif;
elseif($return['affected'] == 0):
    $array_error = array('error' => '1', 'message' => 'Cambios no encontrados');
else:
    $array_error = array('error' => '2', 'message' => 'Existe referencia');
endif;

echo json_encode($array_error);