<?php

$page = "images";
require '../../config.php';
include '../mod_head/index.php';
include '../mod_nav/index.php';

$bd = new Db();
$image = new Image();
$total_images = $image->countTotal();
$rowsPerPage = 8;
$pageNum = 0;
$offset = 0;
$total_paginas = ceil($total_images / $rowsPerPage);
$texto_busqueda = "";
$array_images = $image->searchImage($texto_busqueda,'', $offset, $rowsPerPage);
$category_images = $image->getCategories();

?>

<div class="container contain_table_images_admin">
    <button class="btn btn-modal btn-info">Categorías</button>
    <table class="table table-striped">
        <thead>
            <th>Ref</th>
            <th>Imagen</th>
            <th>Ancho (100pp)</th>
            <th>Alto (100ppp)</th>
            <th>Ancho (300ppp)</th>
            <th>Alto (300ppp)</th>
            <th>Etiquetas</th>
            <th>Categorías</th>
        </thead>
        <tbody class="contentTable">

        <?php

        foreach ($array_images as $image_data):

            ?>
            <tr id_image="<?=$image_data['id']?>">
                <input type="hidden" name="old_refImage" value="<?=$image_data['ref_image']?>" class="old_refImage"/>
                <td class="inline_table"><input readonly class="changeRef tokendfield_readonly" type="text" title="" name="ref_image" value="<?=$image_data['ref_image']?>"/>
                    <i class="activeEdit glyphicon glyphicon-edit btn-lg btn"></i>
                </td>
                <td class="td_preview_image">
                    <a class="image-popup-no-margins" href="<?=$image_data['url_image']?>" title="Etiquetas: <?=$image_data['tags']?>">
                        <img src="<?=$image_data['url_image']?>" class="preview_image">
                    </a>
                </td>
                <td><span><?=$image_data['min_width']?></span></td>
                <td><span><?=$image_data['min_height']?></span></td>
                <td><span><?=$image_data['max_width']?></span></td>
                <td><span><?=$image_data['max_height']?></span></td>
                <td><input class="tokenfield" size="10" type="text" title="" name="tags" readonly value="<?=$image_data['tags']?>"/></td>
                <td>
                </td>
            </tr>
            <?php

        endforeach;

        ?>
</tbody>
</table>
<?php
    if ($total_paginas > 1) {
    echo '<div class="pagination text-center">';
        echo '<ul>';
            for ($i = 1; $i <= $total_paginas; $i++) {
            if ($pageNum == $i)
            echo '<li class="active"><a>' . $i . '</a></li>';
            else
            echo '<li><a class="paginate paginate_image_admin" data="' . $i . '">' . $i . '</a></li>';
            }
            if ($pageNum != $total_paginas)
            echo '<li><a class="paginate paginate_image_admin" data="2"><i class="glyphicon glyphicon-arrow-right"></i></a></li>';
            echo '</ul>';
        echo '</div>';
    }
?>
</div>

<?php

include '../mod_footer/index.php';
include root."/modals/basic_modal.php";

?>