<?php

require '../../config.php';

function insert_files_dir($dir){
    $image = new Image();
    $array_images = [];
    $array_ref_images = [];
    if(is_dir($dir)):
        if($dh = opendir($dir)):
            while (($file = readdir($dh)) !== false):
                if(filetype($dir . $file) == 'file'):
                    $ext =  explode('.',$file)[1];
                    $ppp = pppImages($file);
                    $size = size_image($file);
                    $width = $size[0];
                    $max_width = pxtocms($size[0], 300);
                    $min_width = pxtocms($size[0], 100);
                    $height = $size[1];
                    $max_height = pxtocms($size[1], 300);
                    $min_height = pxtocms($size[1], 100);
                    $ref = $array_images[explode('.jpg',$file)[0]]['ref'] = explode('.jpg',$file)[0];
                    $ref_image = $image->insertImage($ref, path_web.'images/galery/' . $file, $ext, $ppp, $width, $max_width, $min_width, $max_height, $min_height, $height);
                    if(!empty($ref_image)):$array_ref_images[] = $ref_image;endif;
                endif;
            endwhile;
        endif;
    endif;
    return $array_ref_images;
}

function pppImages($imagen){
    $archivo = fopen(root.'images/galery/'.$imagen,'r');
    $cadena = fread($archivo, 50);
    fclose($archivo);
    $datos = bin2hex(substr($cadena,14,4));
    $ppp = substr($datos, 0, 4);
    return hexdec($ppp);
}

function pxtocms($pixeles, $ppp){
    $tamaño_cms = $pixeles / $ppp * 2.54;
    return round($tamaño_cms, 3);
}

function size_image($archivo){
    $imagen = getimagesize(root.'images/galery/'.$archivo);
    $array_medidas = array();
    $array_medidas[0] = $imagen[0];
    $array_medidas[1] = $imagen[1];

    return $array_medidas;
}

$number_images = insert_files_dir(root.'images/galery/');
if(!empty($number_images)):
    foreach ($number_images as $ref_images):
        echo "Insertada imagen con referencia: ".$ref_images."<br>";
    endforeach;
else:
    echo "Ninguna imagen nueva encontrada";
endif;