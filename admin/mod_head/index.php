<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Administración - Estampación Digital Flexol</title>
    <meta name="viewport" content="width=device-width,height=device-height initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>css/tagsinput.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>css/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>external_libraries/toastr-master/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="<?=path_web?>external_libraries/magnific-popup/magnific-popup.css">
    <link rel="stylesheet/less" type="text/css" href="<?=path_web?>css/style.less?v=<?=microtime();?>">
    <link rel="stylesheet/less" type="text/css" href="<?=path_web?>css/style_admin.less?v=<?=microtime();?>">
</head>