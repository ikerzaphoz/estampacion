</body>
<footer id="footer" class="row">
    <img id="img_footer" class="col-xs-12 nopadding" src="<?=path_web?>/images/footer.png">
</footer>
</div>
<script type="application/javascript" src="<?=path_web?>js/jquery.js"></script>
<script type="application/javascript" src="<?=path_web?>js/bootstrap.min.js"></script>
<script type="application/javascript" src="<?=path_web?>js/bootstrap-colorpicker.min.js"></script>
<script type="application/javascript" src="<?=path_web?>external_libraries/jqueryUI/jquery-ui.min.js"></script>
<script type="application/javascript" src="<?=path_web?>external_libraries/toastr-master/toastr.min.js"></script>
<script type="application/javascript" src="<?=path_web?>external_libraries/lazyload/jquery.lazyload.min.js"></script>
<script type="application/javascript" src="<?=path_web?>external_libraries/less/less.min.js"></script>
<script type="application/javascript" src="<?=path_web?>external_libraries/tagsinput/tagsinput.min.js"></script>
<script type="application/javascript" src="<?=path_web?>external_libraries/magnific-popup/magnific-popup.min.js"></script>
<script type="application/javascript" src="<?=path_web?>external_libraries/bootstrapSelect/bootstrap-select.min.js"></script>
<script type="application/javascript" src="<?=path_web?>js/script_admin.js?v=<?= microtime();?>"></script>
